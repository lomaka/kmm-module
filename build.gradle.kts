plugins {
    kotlin("multiplatform") version "1.5.10"
    id("com.chromaticnoise.multiplatform-swiftpackage") version "2.0.3"
    id("com.android.application")
    id("maven-publish")
}

group = "mia.common"
version = "1.0"

repositories {
    google()
    mavenCentral()
    mavenLocal()
}

kotlin {
    android()
    iosX64 {
        binaries {
            framework {
                baseName = "MIACommon"
            }
        }
    }
    iosArm64 {
        binaries {
            framework {
                baseName = "MIACommon"
            }
        }
    }
    sourceSets {
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("com.google.android.material:material:1.4.0")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation("junit:junit:4.13")
            }
        }
        val iosX64Main by getting
        val iosX64Test by getting
        val iosArm64Main by getting
        val iosArm64Test by getting
    }
}

android {
    compileSdkVersion(30)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        applicationId = "mia.common.library"
        minSdkVersion(24)
        targetSdkVersion(30)
    }
}

multiplatformSwiftPackage {
    swiftToolsVersion("5.3")
    targetPlatforms {
        iOS { v("13") }
    }
    packageName("MIACommon")
    zipFileName("MIACommon")
    buildConfiguration { release() }
    distributionMode { local() }
}