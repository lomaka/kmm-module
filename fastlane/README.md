fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## Android
### android test
```
fastlane android test
```
Publish to local maven

----

## iOS
### ios archive
```
fastlane ios archive
```
Create zip with xcframework for ios simulator (i386) and real device ARM64
### ios xcframework
```
fastlane ios xcframework
```
Create xcframework for ios simulator (i386) and real device ARM64
### ios swiftpackage
```
fastlane ios swiftpackage
```
Create Swift package with xcframework for ios simulator (i386) and real device ARM64 as binary target

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
