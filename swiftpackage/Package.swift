// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "MIACommon",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "MIACommon",
            targets: ["MIACommon"]
        ),
    ],
    targets: [
        .binaryTarget(
            name: "MIACommon",
            path: "./MIACommon.xcframework"
        ),
    ]
)
